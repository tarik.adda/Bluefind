/***********************************************************************************************//**
 * \file   main.c
 * \brief  Balise bluefind
 *         Balise qui servira a etre repere grace au systeme bluefind
 **************************************************************************************************/

#ifndef GENERATION_DONE
#error You must run generate first!
#endif

#include <string.h>
#include <stdio.h>
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"

/* BG stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "bg_dfu.h"
#include "gatt_db.h"
#include "aat.h"
#include "infrastructure.h"

/* libraries containing default gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"
#ifdef FEATURE_BOARD_DETECTED
#include "bspconfig.h"
#include "pti.h"
#endif

/* Device initialization header */
#include "InitDevice.h"

//Variable determinant le mode d�v ou prod

bool dev=true;

#ifdef FEATURE_SPI_FLASH
#include "flashpwr.h"
#endif


/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 4
#endif

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

//Debugger de trame (packet tracer)

#ifdef FEATURE_PTI_SUPPORT
static const RADIO_PTIInit_t ptiInit = RADIO_PTI_INIT;
#endif

/* Gecko configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t config = { .config_flags = 0, .sleep.flags =
SLEEP_FLAGS_DEEP_SLEEP_ENABLE, .bluetooth.max_connections =
MAX_CONNECTIONS, .bluetooth.heap = bluetooth_stack_heap, .bluetooth.heap_size =
		sizeof(bluetooth_stack_heap), .gattdb = &bg_gattdb_data,

#ifdef FEATURE_PTI_SUPPORT
		.pti = &ptiInit,
#endif
	};



/* trame d'annonce */
static struct {
	uint8_t flagsLen; /* Taille du champ flag. */
	uint8_t flagsType; /* Type du champ flag. */
	uint8_t flags; /* Champ flag. */
	uint8_t txLen; /* Taille du champ Tx. */
	uint8_t txType; /* Type du champ Tx. */
	uint8_t txPower; /* Puissance de transmission. */
	uint8_t NameLen; /* Taille du champ ShortLocalName. */
	uint8_t NameType; /* Type du champ ShortLocalName. */
	uint8_t Name0; /* ShortLocalName. */
	uint8_t Name1; /* ShortLocalName. */
	uint8_t Name2; /* ShortLocalName. */
	uint8_t Name3; /* ShortLocalName. */
	uint8_t Name4; /* ShortLocalName. */
	uint8_t Name5; /* ShortLocalName. */
	uint8_t Name6; /* ShortLocalName. */
	uint8_t Name7; /* ShortLocalName. */
	uint8_t mandataLen; /* Taille du champ fabricant. */
	uint8_t mandataType; /* Type du champ fabricant. */
	uint8_t compId[2]; /* Mon ID. */
	uint8_t beacType; /* Type de balise. */
	uint8_t family[2]; /* Famille de balise.*/
}bcnBeaconAdvDataDev = {
	/* Flag bits - See Bluetooth 4.0 Core Specification , Volume 3, Appendix C, 18.1 for more details on flags. */
	0x02, /* taille  */
	0x01, /* Flags: LE General Discoverable Mode, BR/EDR is disabled. */
	0x04 | 0x02,

	/* AD struct: TX power level. Servira pour trouver la distance */
	0x02, /* Taille = 2 octets */
	0x0a, /* Type de la structure = 0x0A (TX Power) */
	0x03, /* Puissance allant de -127DBM a 127DBM */

	/* AD struct: ShortLocalName */
	0x09, /* Taille = 9 octets */
	0x09, /* Type de la structure = 0x08 (Short local name) */
	0x42, /* B */
	0x6c, /* l */
	0x75, /* u */
	0x65, /* e */
	0x46, /* F */
	0x69, /* i */
	0x6e, /* n */
	0x44, /* D */

	/* Partie d�di� a la balise */
	0x06, /* Taille = 6 octets (Au pire on ajuste, au minimum 3) */
	0xff, /* Type = Manufacturer Specific Data */
	{	UINT16_TO_BYTE1(0xffff), UINT16_TO_BYTE0(0xffff)}, /* PROJET */
	0x01, /* Type balise */
	{	UINT16_TO_BYTE1(0xb2b3), UINT16_TO_BYTE0(0xb2b3)} /* De la famille de balise X (avec X famille de balise + 3 recepteurs) */
};

/* trame d'annonce */
static struct {
	uint8_t flagsLen; /* Taille du champ flag. */
	uint8_t flagsType; /* Type du champ flag. */
	uint8_t flags; /* Champ flag. */
	uint8_t txLen; /* Taille du champ Tx. */
	uint8_t txType; /* Type du champ Tx. */
	uint8_t txPower; /* Puissance de transmission. */
	uint8_t mandataLen; /* Taille du champ fabricant. */
	uint8_t mandataType; /* Type du champ fabricant. */
	uint8_t compId[2]; /* Mon ID. */
	uint8_t beacType; /* Type de balise. */
	uint8_t family[2]; /* Famille de balise.*/
} bcnBeaconAdvDataProd = {
/* Flag bits - See Bluetooth 4.0 Core Specification , Volume 3, Appendix C, 18.1 for more details on flags. */
0x02, /* taille  */
0x01, /* Flags: LE General Discoverable Mode, BR/EDR is disabled. */
0x04 | 0x02,

/* AD struct: TX power level. Servira pour trouver la distance */
0x02, /* Taille = 2 octets */
0x0a, /* Type de la structure = 0x0A (TX Power) */
0x03, /* Puissance allant de -127DBM a 127DBM */

/* Partie d�di� a la balise */
0x06, /* Taille = 6 octets (Au pire on ajuste, au minimum 3) */
0xff, /* Type = Manufacturer Specific Data */
{ UINT16_TO_BYTE1(0xffff), UINT16_TO_BYTE0(0xffff) }, /* PROJET */
0x01, /* Type balise */
{ UINT16_TO_BYTE1(0xb2b3), UINT16_TO_BYTE0(0xb2b3) } /* De la famille de balise X (avec X famille de balise + 3 recepteurs) */
};

static uint8_t lenDev = sizeof(bcnBeaconAdvDataDev);
static uint8_t *pDataDev = (uint8_t*) (&bcnBeaconAdvDataDev);


static uint8_t lenProd = sizeof(bcnBeaconAdvDataProd);
static uint8_t *pDataProd = (uint8_t*) (&bcnBeaconAdvDataProd);

/*
 //Concerne la trame de scan
 static uint8_t lenS;
 static uint8_t *pDataS;
 */

/* variable concernant le nom de la balise */
//char nom[27];
/**
 * @Brief Fonction pour cr�er ma trame d'annonce
 *
 * La fonction construit ma trame selon un format pr�d�finit
 * Configure l'appareil pour la transmettre avec les diff�rents parametres choisi.
 */


void bcnSetupAdvBeaconingDev(void) {

	/* Cette fonction d�finit une trame pr�d�finit selon mes normes � moi Tarik theboss13013xxDarkItachi
	 * La trame sera d'une taille de 13 octets (on peut aller jusqu'a plus si nec�ssaire).
	 */

	/* On fixe la transmission */
	gecko_cmd_system_set_tx_power(0);

	/* Parametre la trame d'annonce */
	gecko_cmd_le_gap_set_adv_data(0, lenDev, pDataDev);

	/* 100ms d'interval d'annonce. On utilise tous les canaux.
	 * The first two parameters are minimum and maximum advertising interval, both in
	 * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
	gecko_cmd_le_gap_set_adv_parameters(480, 480, 7);

	/* Met le mode sp�cifique � ma trame et en mode scannable */
	gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);

}

void bcnSetupAdvBeaconingProd(void) {

	/* Cette fonction d�finit une trame pr�d�finit selon mes normes � moi Tarik theboss13013xxDarkItachi
	 * La trame sera d'une taille de 13 octets (on peut aller jusqu'a plus si nec�ssaire).
	 */

	/* On fixe la transmission */
	gecko_cmd_system_set_tx_power(0);

	/* Parametre la trame d'annonce */
	gecko_cmd_le_gap_set_adv_data(0, lenProd, pDataProd);

	/* 100ms d'interval d'annonce. On utilise tous les canaux.
	 * The first two parameters are minimum and maximum advertising interval, both in
	 * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
	gecko_cmd_le_gap_set_adv_parameters(480, 480, 7);


	/* Met le mode sp�cifique � ma trame et en mode scannable */
	gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);
}

/**
 * @brief fonction pour initialiser la balise avec un nom unique afin qu'elle soit rep�rable et identifiable
 */
/*
 void nommage(void) {
 //Variable stockant l'adresse mac
 struct gecko_msg_system_get_bt_address_rsp_t* btAddr;
 btAddr = gecko_cmd_system_get_bt_address();

 //Formatage du nom de la balise
 snprintf(nom, 27, "BlueFinD:%02x:%02x:%02x:%02x:%02x:%02x",
 btAddr->address.addr[5], btAddr->address.addr[4],
 btAddr->address.addr[3], btAddr->address.addr[2],
 btAddr->address.addr[1], btAddr->address.addr[0]);

 //Ecriture dans le gatt local du nom
 gecko_cmd_gatt_server_write_attribute_value(gattdb_device_name, 0, 27,
 (uint8_t *) nom);

 // trame de scan
 struct {
 uint8_t NameLen; // Taille du champ Complete Local Name.
 uint8_t NameType; // Type du champ Complete Local Name.
 uint8_t Name0; // Complete Local Name.
 uint8_t Name1; // Complete Local Name.
 uint8_t Name2; // Complete Local Name.
 uint8_t Name3; // Complete Local Name.
 uint8_t Name4; // Complete Local Name.
 uint8_t Name5; // Complete Local Name.
 uint8_t Name6; // Complete Local Name.
 uint8_t Name7; // Complete Local Name.
 uint8_t Name8; // Complete Local Name.
 uint8_t Name9[2]; // Complete Local Name.
 uint8_t Name10; // Complete Local Name.
 uint8_t Name11[2]; // Complete Local Name.
 uint8_t Name12; // Complete Local Name.
 uint8_t Name13[2]; // Complete Local Name.
 uint8_t Name14; // Complete Local Name.
 uint8_t Name15[2]; // Complete Local Name.
 uint8_t Name16; // Complete Local Name.
 uint8_t Name17[2]; // Complete Local Name.
 uint8_t Name18; // Complete Local Name.
 uint8_t Name19[2]; // Complete Local Name.
 } bcnBeaconScnData = {
 // AD struct: Complete Local Name.
 0x1b, // Taille = 27 octets
 0x09, // Type de la structure = 0x08 (Short local name)
 0x42, // B
 0x6c, // l
 0x75, // u
 0x65, // e
 0x46, // F
 0x69, // i
 0x6e, // n
 0x44, // D
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[5]), UINT16_TO_BYTE0(
 btAddr->address.addr[5]) }, // XX
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[4]), UINT16_TO_BYTE0(
 btAddr->address.addr[4]) }, // XX
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[3]), UINT16_TO_BYTE0(
 btAddr->address.addr[3]) }, // XX
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[2]), UINT16_TO_BYTE0(
 btAddr->address.addr[2]) }, // XX
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[1]), UINT16_TO_BYTE0(
 btAddr->address.addr[1]) }, // XX
 0x3a, // :
 { UINT16_TO_BYTE1(btAddr->address.addr[0]), UINT16_TO_BYTE0(
 btAddr->address.addr[0]) }, // XX
 };
 //On recupere la taille et la trame
 lenS = sizeof(bcnBeaconScnData);
 pDataS = (uint8_t*) (&bcnBeaconScnData);

 }
 */
/**
 * @brief  Main function
 */
void main(void) {

#ifdef FEATURE_SPI_FLASH
	/* Put the SPI flash into Deep Power Down mode for those radio boards where it is available */
	flashpwrInit();
	flashpwrDeepPowerDown();
#endif

	//Interruption initialis� sur le pin PF2
	GPIO_ExtIntConfig(gpioPortF, 2, 2, true, true, true);

	/* Initialize peripherals */
	enter_DefaultMode_from_RESET();

	/* Initialize stack */
	gecko_init(&config);

	while (1) {
		struct gecko_cmd_packet* evt;

		/* Check for stack event. */
		evt = gecko_wait_event();
		/* Run application and event handler. */

		switch (BGLIB_MSG_ID(evt->header)) {

		/*  This boot event is generated when the system is turned on or reseted. */
		case gecko_evt_system_boot_id:
			/* On initialise la trame */
			//Initialisation de la balise
			//nommage();
			//Parametrage de la trame d'annonce et scan
			if (dev==true)
			bcnSetupAdvBeaconingDev();
			else bcnSetupAdvBeaconingProd();
			break;

			//Si une intteruption est d�tect�
		case gecko_evt_hardware_interrupt_id:
			if(dev==true)
				dev=false;
			else dev=true;

			break;

		default:
			break;
		}
	}
}

/** @} (end addtogroup app) */
/** @} (end addtogroup Application) */

